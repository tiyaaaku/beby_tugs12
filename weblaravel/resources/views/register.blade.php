<!-- resources/views/register.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Registration Form</h1>
    <form action="{{ url('/register') }}" method="post">
        @csrf
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" required>
        <br>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" required>
        <br>
        <label for="gender">Gender:</label>
        <select name="gender" required>
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
        </select>
        <br>
        <label for="nationality">Nationality:</label>
        <input type="text" name="nationality" required>
        <br>
        <!-- Add other form fields as needed -->
        <br>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
